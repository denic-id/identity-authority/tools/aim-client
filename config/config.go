package config

type AppConfig struct {
	Client       *ClientConfig `json:"client" yaml:"client"`
	AimURL       string        `json:"aimURL" yaml:"aimURL"`
	AuthzDir     string        `json:"authzDir" yaml:"authzDir"`
	AuthorityURL string        `json:"authorityURL" yaml:"authorityURL"`
	DnsHookCmd   string        `json:"dnsHookCmd" yaml:"dnsHookCmd"`
	MailHookCmd  string        `json:"mailHookCmd" yaml:"mailHookCmd"`
	DnsConfig    DnsConfig     `json:"dns,omitempty" yaml:"dns,omitempty"`
	MailConfig   MailConfig    `json:"mail,omitempty" yaml:"mail,omitempty"`
}

type ClientConfig struct {
	ID        string `json:"id" yaml:"id"`
	KeyFile   string `json:"keyFile" yaml:"keyFile"`
	Name      string `json:"name" yaml:"name"`
	FullName  string `json:"fullName" yaml:"fullName"`
	Email     string `json:"email" yaml:"email"`
	IssuerURL string `json:"issuerURL" yaml:"issuerURL"`
}

type DnsConfig interface{}
type MailConfig interface{}
