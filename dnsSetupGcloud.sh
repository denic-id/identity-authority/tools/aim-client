#!/bin/bash
set -x
rname="$1"
rtype="$2"
rdata="$3"
ZONE=mydenic-de
TTL=300

trap "{ rm -f transaction.yaml; }" EXIT

DNS_ZONE=$(gcloud dns managed-zones list | grep ${ZONE} | awk '{print $2}')
[[ -z "${DNS_ZONE}" ]] && { echo "Failed to get dns name for zone ${ZONE}"; exit 1; }

echo "Starting transaction."
gcloud dns record-sets transaction start --zone=${ZONE}
[[ $? -ne 0 ]] && { echo "failed to start transaction for ${rname} in zone ${ZONE}"; exit 1; }

EXISTING_RECORD=$(gcloud dns record-sets list --zone=${ZONE} --name=${rname}. --type=TXT --format="get(name,type,ttl,DATA)")
[[ -n "$EXISTING_RECORD" ]] && {
    echo $EXISTING_RECORD | while read n t ttl d ; do
        echo "appending remove for old record to transaction."
        gcloud dns record-sets transaction remove "$d" --zone=${ZONE} --name=${n} --ttl=${ttl} --type=${t}
        [[ $? -ne 0 ]] && { echo "failed to add remove to transaction for ${rname} in zone ${ZONE}"; exit 1; }
    done
}

echo "appending add for new record to transaction"
gcloud dns record-sets transaction add "$rdata" --zone=${ZONE} --name=${rname}. --ttl=${TTL} --type=${rtype}
[[ $? -ne 0 ]] && { echo "failed to add remove to transaction for ${rname} in zone ${ZONE}"; exit 1; }

echo "executing transaction"
TRANSACTION_ID=$(gcloud dns record-sets transaction execute --zone=${ZONE} --format="get(id)")
[[ $? -ne 0 ]] && { echo "failed to execute transaction for ${rname} in zone ${ZONE}"; exit 1; }

echo "check if the transaction is done"
while true; do
    STATUS=$(gcloud dns record-sets changes describe --zone=mydenic-de ${TRANSACTION_ID} --format="get(status)")
    [[ $? -ne 0 ]] && { echo "failed to check transaction ${TRANSACTION_ID} in zone ${ZONE}"; exit 1; }
    [[ "${STATUS}" = "done" ]] && { break; }
    sleep 1
done

echo "update done. waiting for zone to published."
NSERVERS=$(gcloud dns record-sets list --zone=${ZONE} --name=${DNS_ZONE} --format="get(DATA)" --type=NS | sed -e 's/;/ /g')
# we need to iterate over all dns servers.


while true; do
    updated=true
    for ns in ${NSERVERS}; do
        VALUE=$(dig @${ns} TXT ${rname} +short)
        if [ "${VALUE}" != "\"${rdata}\"" ]; then updated=false; fi
    done
    if ${updated}; then break; fi
    sleep 1
done
echo "Done."
