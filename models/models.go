package models

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"time"

	yaml "gopkg.in/yaml.v2"
)

type Agent struct {
	ID        string      `json:"id,omitempty"`
	JWK       interface{} `json:"jwk,omitempty"`
	Name      string      `json:"name"`
	FullName  string      `json:"fullName" yaml:"fullName"`
	Email     string      `json:"email"`
	Status    string      `json:"status"`
	IssuerURL string      `json:"issuerURL"`
}

type IdentityAuthzRequest struct {
	Identifier string `json:"identifier"`
	Locale     string `json:"locale"`
}

// IdentityAuthz represents the database and application model for an identity authorization.
type IdentityAuthz struct {
	ID         int64     `json:"id" yaml:"id"`
	Identifier string    `json:"identifier" yaml:"identifier"`
	Email      string    `json:"-" yaml:"email,omitempty"`
	Expires    time.Time `json:"expires" yaml:"expires"`
	Status     string    `json:"status" yaml:"status"`
	Challenge  Challenge `json:"challenge" yaml:"challenge"`
}

// Challenge represents the database and application model for a challenge that is embedded in the IdentityAuthz.
type Challenge struct {
	URL       string     `json:"url" yaml:"url"`
	Token     string     `json:"token" yaml:"token"`
	Validated *time.Time `json:"validated" yaml:"validated"`
}

// Identity represents the database and application model for an identity.
type Identity struct {
	Identifier     string `json:"identifier" yaml:"identifier"`
	Subject        string `json:"sub" yaml:"sub"`
	Locale         string `json:"locale" yaml:"locale"`
	Email          string `json:"-" yaml:"email,omitempty"`
	URL            string `json:"url" yaml:"url"`
	URLExpires     int64  `json:"url_expire" yaml:"urlExpire"`
	AgentIssuerURL string `json:"agentIssuerURL" yaml:"agentIssuerURL"`
}

type IdentitiesCredentialsResetResponse struct {
	URL          string `json:"url" yaml:"url"`
	ResetExpires int64  `json:"reset_expire" yaml:"urlExpire"`
}

func AuthzFromYamlFile(basepath, id string) (*IdentityAuthz, error) {

	filename := fmt.Sprintf(basepath + "/" + id + ".yaml")

	b, err := ioutil.ReadFile(filepath.Clean(filename))
	if err != nil {
		return nil, err
	}

	authz := new(IdentityAuthz)
	err = yaml.Unmarshal(b, authz)
	if err != nil {
		return nil, err
	}
	return authz, nil
}
