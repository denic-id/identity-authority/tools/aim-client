# aim-client

## Latest releases
[\_\_release\_linux\_\_](releases/\_\_release\_linux\_\_)  
[\_\_release\_linux\_\_.sha256sum](releases/\_\_release\_linux\_\_.sha256sum)  
[\_\_release\_windows\_\_](releases/\_\_release\_windows\_\_)  
[\_\_release\_windows\_\_.sha256sum](releases/\_\_release\_windows\_\_.sha256sum)

## Source code
[\_\_release\_source\_\_](releases/\_\_release\_source\_\_)  
[\_\_release\_source\_\_.sha256sum](releases/\_\_release\_source\_\_.sha256sum)

## License
aim-client is licensed under the terms of the MIT License.  
See the repositories' [LICENSE](https://gitlab.com/denic-id/aim-client/raw/master/LICENSE) file for further information.

