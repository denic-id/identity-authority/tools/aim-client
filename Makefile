APPNAME := aim-client
APPSRC := ./cmd/$(APPNAME)

BASEDIR := $(shell pwd)

GITCOMMITHASH := $(shell git log --max-count=1 --pretty="format:%h" HEAD)
GITCOMMIT := -X main.gitcommit=$(GITCOMMITHASH)

VERSIONTAG := $(shell git describe --tags --abbrev=0)
VERSION := -X main.appversion=$(VERSIONTAG)

BUILDTIMEVALUE := $(shell date +%Y-%m-%dT%H:%M:%S%z)
BUILDTIME := -X main.buildtime=$(BUILDTIMEVALUE)

LDFLAGS := '-extldflags "-static" -d -s -w $(GITCOMMIT) $(VERSION) $(BUILDTIME)'
LDFLAGS_WINDOWS := '-extldflags "-static" -s -w $(GITCOMMIT) $(VERSION) $(BUILDTIME)'
all:info clean build

clean:
	@echo Cleaning up
	rm -rf build
	rm -rf release
	rm -rf public

info: 
	@echo - appname:   $(APPNAME)
	@echo - version:   $(VERSIONTAG)
	@echo - commit:    $(GITCOMMITHASH)
	@echo - buildtime: $(BUILDTIMEVALUE) 

dep:
	@go get -v -d ./...

build-windows: info dep
	@echo Building for windows
	@mkdir -p build/windows
	@CGO_ENABLED=0 \
	GOOS=windows \
	go build -o build/windows/$(APPNAME)-$(VERSIONTAG).exe -a -ldflags $(LDFLAGS_WINDOWS) $(APPSRC)
	@cd $(BASEDIR)/build/windows && \
	sha256sum $(APPNAME)-$(VERSIONTAG).exe > $(APPNAME)-$(VERSIONTAG).exe.sha256sum

build-linux: info dep
	@echo Building for linux
	@mkdir -p build/linux
	CGO_ENABLED=0 \
	GOOS=linux \
	go build -o build/linux/$(APPNAME)-$(VERSIONTAG)-$(GITCOMMITHASH)-linux_x86_64 -a -ldflags $(LDFLAGS) $(APPSRC)
	@cp build/linux/$(APPNAME)-$(VERSIONTAG)-$(GITCOMMITHASH)-linux_x86_64 build/linux/$(APPNAME)
	@cp build/linux/$(APPNAME)-$(VERSIONTAG)-$(GITCOMMITHASH)-linux_x86_64 build/linux/$(APPNAME)-$(VERSIONTAG)-linux_x86_64
	cd build/linux && \
	sha256sum $(APPNAME)-$(VERSIONTAG)-linux_x86_64 > $(APPNAME)-$(VERSIONTAG)-linux_x86_64.sha256sum
install-linux: build-linux
	@cp build/linux/$(APPNAME) $$GOPATH/bin/

image: 
	@echo Creating docker image
	@docker build -t $(APPNAME):$(VERSIONTAG)-$(GITCOMMITHASH) .

release-linux:
	@mkdir -p release/linux
	@echo Bundle linux release $(VERSIONTAG)
	@echo -n "verify checksum: " 
	@cd build/linux && \
	sha256sum -c $(APPNAME)-$(VERSIONTAG)-linux_x86_64.sha256sum
	
	@cd build/linux && \
	tar cfz ../../release/linux/$(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz $(APPNAME)-$(VERSIONTAG)-linux_x86_64
	@cd release/linux && \
	sha256sum $(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz > $(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz.sha256sum
	@echo archive created: $(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz

release-windows:
	@mkdir -p release/windows
	@echo Bundle windows release $(VERSIONTAG)
	@echo -n "verify checksum: " 
	@cd build/windows && \
	sha256sum -c $(APPNAME)-$(VERSIONTAG).exe.sha256sum
	
	@echo adding zip package
	@apt-get update && apt-get install -y zip

	@cd build/windows && \
	zip ../../release/windows/$(APPNAME)-$(VERSIONTAG)-windows.zip $(APPNAME)-$(VERSIONTAG).exe
	@cd release/windows && \
	sha256sum $(APPNAME)-$(VERSIONTAG)-windows.zip > $(APPNAME)-$(VERSIONTAG)-windows.zip.sha256sum
	@echo archive created: $(APPNAME)-$(VERSIONTAG)-windows.zip

update-releasepage:
	mkdir -p public/releases
	git archive HEAD | gzip > public/releases/$(APPNAME)-$(VERSIONTAG).src.tar.gz
	@cd public/releases && \
	sha256sum $(APPNAME)-$(VERSIONTAG).src.tar.gz > $(APPNAME)-$(VERSIONTAG).src.tar.gz.sha256sum
	cp release/linux/$(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz public/releases
	cp release/linux/$(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz.sha256sum public/releases
	cp release/windows/$(APPNAME)-$(VERSIONTAG)-windows.zip public/releases
	cp release/windows/$(APPNAME)-$(VERSIONTAG)-windows.zip.sha256sum public/releases
	markdown page/index-body.md > page/index-body.tpl
	cat page/index-header.tpl page/index-body.tpl page/index-footer.tpl > public/index.html 
	sed -i 's/__release_linux__/$(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz/g' public/index.html
	sed -i 's/__release_windows__/$(APPNAME)-$(VERSIONTAG)-windows.zip/g' public/index.html
	sed -i 's/__release_source__/$(APPNAME)-$(VERSIONTAG).src.tar.gz/g' public/index.html