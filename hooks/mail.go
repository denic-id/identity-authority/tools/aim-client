package hooks

import (
	"os/exec"
)

// MailCommand stores the attributes for invoking the hook which sends mails.
type MailCommand struct {
	ProgramPath  string
	Identifier   string
	EmailAddress string
	AgentName    string
	MagicURL     string
	Locale       string
}

// NewMailCommand creates a new MailCommand with the passed params.
func NewMailCommand(prog, id, address, agentName, magicURL, locale string) *MailCommand {
	if len(prog) < 1 ||
		len(id) < 1 ||
		len(address) < 1 ||
		len(agentName) < 1 ||
		len(magicURL) < 1 ||
		len(locale) < 1 {

		return nil
	}

	return &MailCommand{
		ProgramPath:  prog,
		Identifier:   id,
		EmailAddress: address,
		AgentName:    agentName,
		MagicURL:     magicURL,
		Locale:       locale,
	}
}

// Invoke runs the MailCommand
func (mailCmd *MailCommand) Invoke() ([]byte, error) {
	// #nosec
	cmd := exec.Command(mailCmd.ProgramPath, mailCmd.Identifier, mailCmd.EmailAddress, mailCmd.AgentName, mailCmd.MagicURL, mailCmd.Locale)
	return cmd.Output()
}
