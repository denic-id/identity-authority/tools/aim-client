package hooks

import (
	"os/exec"
)

type DNSCommand struct {
	ProgramPath string
	Name        string
	RType       string
	RData       string
}

// NewDNSCommand creates a new DnsCommand with the passed params.
func NewDNSCommand(prog, name, rtype, rdata string) *DNSCommand {
	if len(prog) < 1 ||
		len(name) < 1 ||
		len(rtype) < 1 ||
		len(rdata) < 1 {

		return nil
	}

	return &DNSCommand{
		ProgramPath: prog,
		Name:        name,
		RType:       rtype,
		RData:       rdata,
	}
}

func (dnsCmd *DNSCommand) Invoke() ([]byte, error) {

	// #nosec
	cmd := exec.Command(dnsCmd.ProgramPath, dnsCmd.Name, dnsCmd.RType, dnsCmd.RData)
	return cmd.Output()
}
