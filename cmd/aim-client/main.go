package main

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	gurl "net/url"

	"gitlab.com/denic-id/aim-client/hooks"

	yaml "gopkg.in/yaml.v2"

	"gitlab.com/denic-id/aim-client/utils"

	"gitlab.com/denic-id/aim-client/client"
	"gitlab.com/denic-id/aim-client/config"

	"github.com/sebidude/configparser"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

var (
	gitcommit  string
	appversion string
	buildtime  string

	configfile string
	app        *kingpin.Application

	id     string
	locale string
	url    string

	name     string
	fullname string
	email    string
)

func init() {
	app = kingpin.New("aim-client", "Commandline app for interacting with AIM.")
	app.Flag("config", "full path to the config file to be used.").Short('c').Default("config.yaml").Envar("AIM_CLIENT_CONFIG").StringVar(&configfile)
	init := app.Command("init", "Inititalize a new agent.")
	init.Flag("url", "url of the authority").Default("https://id.test.denic.de/aim/v1").Short('u').Envar("AIM_CLIENT_URL").StringVar(&url)

	create := app.Command("create", "Create a resource")
	createAuthz := create.Command("authz", "Create a new identity authz")
	createAuthz.Arg("id", "the identitfier which should be created.").Required().StringVar(&id)
	createAuthz.Flag("locale", "the desired locale for the id").Short('l').Default("en").StringVar(&locale)
	createAuthz.Flag("email", "the mail address of the identity owner").Short('m').Default("").StringVar(&email)

	createID := create.Command("id", "Setup everything for a new identity.")
	createID.Arg("id", "the identitfier which should be created.").Required().StringVar(&id)
	createID.Flag("locale", "the desired locale for the id").Short('l').Default("en").StringVar(&locale)
	createID.Flag("email", "the mail address of the identity owner").Short('m').Default("").StringVar(&email)

	activate := app.Command("activate", "Signals that the ACME challenge for the id is set up.")
	activateID := activate.Command("id", "Signals that the ACME challenge for the id is set up.")
	activateID.Arg("id", "the identifier for which the ACME challenge is set up.").Required().StringVar(&id)

	update := app.Command("update", "Update a resource")
	updateAgent := update.Command("agent", "update an agent")
	updateAgent.Flag("name", "New name for the agent").StringVar(&name)
	updateAgent.Flag("fullname", "New full name for the agent").StringVar(&fullname)
	updateAgent.Flag("email", "New email for the agent").StringVar(&email)

	get := app.Command("get", "get a resource")
	get.Command("agent", "get the agent stored at the authority")

	getAuthz := get.Command("authz", "get the authorization for an identity.")
	getAuthz.Arg("id", "identifier to get").Required().StringVar(&id)

	getIdentity := get.Command("id", "get the identity.")
	getIdentity.Arg("id", "identifier to get").Required().StringVar(&id)

	reset := app.Command("reset", "Get a new password reset link for an identity.")
	resetIdentity := reset.Command("id", "Get a new password reset link for an identity.")
	resetIdentity.Arg("id", "identifier for which the password reset url should be generated.").Required().StringVar(&id)

	dnsSetup := app.Command("dnssetup", "setup up the dns for an identity")
	acmeSetup := dnsSetup.Command("acme", "setup the acme-challenge for an identifier")
	acmeSetup.Arg("id", "identifier to setup the acme-challenge for").Required().StringVar(&id)

	openidSetup := dnsSetup.Command("openid", "setup the openid record for an identifier")
	openidSetup.Arg("id", "identifier to setup the openid record for").Required().StringVar(&id)

	mail := app.Command("mail", "Send the mail with the setup URL to the identity's mail address.")
	mail.Arg("id", "identifier for which the mail should be sent.").Required().StringVar(&id)

	app.Command("licenses", "Print licenses from libraries used in this binary.")
	app.Command("version", "Print version info.")
	app.Command("config", "Print the current config.")
}

func main() {

	operation := kingpin.MustParse(app.Parse(os.Args[1:]))

	switch operation {
	case "config":
		PrintConfig()
	case "licenses":
		PrintLicenses()
	case "version":
		PrintVersion()
	case "init":
		InitAgent()
		fmt.Println("Success")
	case "create authz":
		CreateIdAuthz()
		fmt.Println("Success")
	case "create id":
		CreateIdentity()
		fmt.Println("Success")
	case "get authz":
		GetIdAuthz()
		fmt.Println("Success")
	case "get id":
		GetIdentity()
		fmt.Println("Success")
	case "activate id":
		ActivateID()
		fmt.Println("Success")
	case "reset id":
		ResetIdentityCredentials()
		fmt.Println("Success")
	case "update agent":
		UpdateAgent()
		fmt.Println("Success")
	case "get agent":
		GetAgent()
		fmt.Println("Success")
	case "dnssetup acme":
		SetupAcme()
		fmt.Println("Success")
	case "dnssetup openid":
		SetupOpenID()
		fmt.Println("Success")
	case "mail":
		SendMail()
		fmt.Println("Success")
	default:
		fmt.Printf("%s not implemented yet.", operation)
	}
}

func checkError(err error) {
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}

func PrintVersion() {
	fmt.Println("AIM Client - Commandline client for AIM")
	fmt.Printf("Version   : %s\n", appversion)
	fmt.Printf("Git commit: %s\n", gitcommit)
	fmt.Println("License   : MIT")
	fmt.Printf("Build time: %s\n", buildtime)

}

func PrintLicenses() {
	PrintVersion()
	fmt.Println(licenseInfo)
}

func InitAgent() {

	fmt.Println("Initializing a new agent")
	var existingConfig config.AppConfig
	err := configparser.ParseYaml(configfile, &existingConfig)
	if err != nil {
		fmt.Println("Cannot read configfile: %s", err.Error())
	}
	newconfigfile := askForValue("Full where the new config should be stored.", configfile)
	if newconfigfile != configfile {
		existingConfig = config.AppConfig{
			Client: &config.ClientConfig{
				Name:     "",
				FullName: "",
				Email:    "",
				KeyFile:  "",
			},
			AimURL:       "https://id.staging.denic.de/aim/v1",
			AuthzDir:     "",
			AuthorityURL: "https://id.staging.denic.de",
			DnsHookCmd:   "",
			MailHookCmd:  "",
		}
	}
	name := askForValue("Name for the agent", existingConfig.Client.Name)
	fullname := askForValue("Full name for the agent", name)
	email := askForValue("Email address of the agent", existingConfig.Client.Email)
	issuerURL := askForValue("IssuerURL of the agent", existingConfig.Client.IssuerURL)
	keyfile := askForValue("Path to write the keyfile to", existingConfig.Client.KeyFile)
	aimurl := askForValue("AIM endpoint at the authority ", existingConfig.AimURL)
	authorityURL := askForValue("BaseURL of the authority", existingConfig.AuthorityURL)
	authzdir := askForValue("Directory where identity authz are stored", existingConfig.AuthzDir)
	dnsCmd := askForValue("Command to be invoked for setting up dns records.", existingConfig.DnsHookCmd)
	mailCmd := askForValue("Command to be invoked for sending a mail to the identity owner", existingConfig.MailHookCmd)
	id := ""
	if len(existingConfig.Client.ID) > 0 {
		id = existingConfig.Client.ID
	}
	appconfig := config.AppConfig{
		Client: &config.ClientConfig{
			ID:        id,
			Name:      name,
			FullName:  fullname,
			Email:     email,
			KeyFile:   keyfile,
			IssuerURL: issuerURL,
		},
		AimURL:       aimurl,
		AuthzDir:     authzdir,
		AuthorityURL: authorityURL,
		DnsHookCmd:   dnsCmd,
		MailHookCmd:  mailCmd,
		DnsConfig:    existingConfig.DnsConfig,
		MailConfig:   existingConfig.MailConfig,
	}

	fmt.Println("Creating directories.")
	for _, dirname := range []string{appconfig.AuthzDir, filepath.Dir(appconfig.Client.KeyFile)} {
		err := utils.MakeDirs(dirname)
		checkError(err)
	}

	fmt.Printf("Store configuration at %s\n", newconfigfile)
	err = utils.ToYamlFile(appconfig, newconfigfile)
	checkError(err)

	fmt.Printf("Creating agent \"%s\" at %s\n", name, aimurl)
	_, err = client.Init(&appconfig, newconfigfile)
	checkError(err)

	if runtime.GOOS == "windows" {
		fmt.Printf("\nrun: \nset AIM_CLIENT_CONFIG=%s\nsetx AIM_CLIENT_CONFIG \"%s\"", newconfigfile, newconfigfile)
	} else {
		fmt.Printf("\nrun: export AIM_CLIENT_CONFIG=%s\n", newconfigfile)
	}
}

func newClient() (*client.Client, error) {
	var appconfig config.AppConfig
	err := configparser.ParseYaml(configfile, &appconfig)
	if err != nil {
		return nil, err
	}

	if len(appconfig.Client.IssuerURL) < 1 {
		return nil, fmt.Errorf("issuerURL is missing in client config. Please update your config and then update the agent.")
	}

	c, err := client.NewFromConfig(&appconfig, configfile)
	if err != nil {
		return nil, err
	}
	return c, nil
}

func PrintConfig() {
	c, err := newClient()
	checkError(err)

	printObj(c.GetConfig())

}

func GetAgent() {
	c, err := newClient()
	checkError(err)

	agent, err := c.GetOwnAgent()
	checkError(err)
	printObj(agent)
}

func UpdateAgent() {
	c, err := newClient()
	checkError(err)

	agent, err := c.UpdateAgent(name, fullname, email)
	checkError(err)

	conf := c.GetConfig()
	conf.Client.Email = agent.Email
	conf.Client.Name = agent.Name
	conf.Client.FullName = agent.FullName
	conf.Client.ID = agent.ID

	err = utils.ToYamlFile(conf, configfile)
	checkError(err)

	printObj(agent)

}

func CreateIdAuthz() {
	c, err := newClient()
	checkError(err)

	_, err = c.CreateAndSaveIdentityAuthz(id, locale, email)
	checkError(err)
	GetIdAuthz()

}

func SendMail() {
	c, err := newClient()
	checkError(err)

	if len(c.GetConfig().MailHookCmd) < 1 {
		fmt.Println("no mailHookCmd is configured. Exit.")
		os.Exit(0)
	}

	identity, err := c.GetIdentity(id)
	checkError(err)

	if len(identity.Email) < 1 {
		fmt.Printf("Identity %s has no email address. Exit.", id)
		os.Exit(1)
	}

	mailCmd := hooks.NewMailCommand(
		c.GetConfig().MailHookCmd,
		identity.Identifier,
		identity.Email,
		c.GetConfig().Client.FullName,
		identity.URL,
		identity.Locale)

	fmt.Printf(
		"Invoking cmd: %s %s %s %s %s %s\n",
		mailCmd.ProgramPath,
		mailCmd.Identifier,
		mailCmd.EmailAddress,
		mailCmd.AgentName,
		mailCmd.MagicURL,
		mailCmd.Locale)

	out, err := mailCmd.Invoke()
	fmt.Println(string(out))
	checkError(err)
}

func SetupAcme() {

	c, err := newClient()
	checkError(err)

	if len(c.GetConfig().DnsHookCmd) < 1 {
		fmt.Println("no dnsHookCmd is configured. Exit.")
		os.Exit(1)
	}

	authz, err := c.GetIdentityAuthz(id)
	checkError(err)

	fmt.Printf("Setting up ACME DNS for %s\n", authz.Identifier)
	challenge, err := c.CreateAcmeChallenge(authz.Challenge.Token)
	checkError(err)
	dnsCmd := hooks.NewDNSCommand(c.GetConfig().DnsHookCmd, "_acme-challenge."+authz.Identifier, "TXT", challenge)
	fmt.Printf("Invoking cmd: %s %s %s %s\n", dnsCmd.ProgramPath, dnsCmd.Name, dnsCmd.RType, dnsCmd.RData)
	out, err := dnsCmd.Invoke()
	fmt.Println(string(out))
	checkError(err)

}

func SetupOpenID() {

	c, err := newClient()
	checkError(err)

	if len(c.GetConfig().DnsHookCmd) < 1 {
		fmt.Println("no dnsCommand is configured. Exit.")
		os.Exit(1)
	}

	authz, err := c.GetIdentityAuthz(id)
	checkError(err)

	u, err := gurl.Parse(c.GetConfig().AimURL)
	checkError(err)
	data := fmt.Sprintf("v=OID1;iss=%s://%s", u.Scheme, u.Hostname())

	fmt.Printf("Setting up openid DNS for %s\n", authz.Identifier)
	dnsCmd := hooks.NewDNSCommand(c.GetConfig().DnsHookCmd, "_openid."+authz.Identifier, "TXT", data)
	fmt.Printf("Invoking cmd: %s %s %s %s\n", dnsCmd.ProgramPath, dnsCmd.Name, dnsCmd.RType, dnsCmd.RData)
	out, err := dnsCmd.Invoke()
	fmt.Println(string(out))
	checkError(err)
}

func GetIdAuthz() {
	c, err := newClient()
	checkError(err)

	authz, err := c.GetIdentityAuthz(id)
	checkError(err)

	printObj(authz)
	ac, err := c.CreateAcmeChallenge(authz.Challenge.Token)
	checkError(err)

	fmt.Printf("ACME Challenge Record: _acme-challenge.%s.  IN TXT \"%s\"\n", authz.Identifier, ac)

}

func ActivateID() {
	c, err := newClient()
	checkError(err)

	authz, err := c.PostAcmeSetupDone(id)
	checkError(err)
	printObj(authz)
}

func GetIdentity() {
	c, err := newClient()
	checkError(err)

	identity, err := c.GetIdentity(id)
	checkError(err)

	printObj(identity)
}

func CreateIdentity() {
	fmt.Printf("Setting up identity %q\n\n", id)
	fmt.Printf("Create authorization for %q\n", id)
	CreateIdAuthz()
	fmt.Println("----- ACME DNS SETUP -----")
	SetupAcme()
	fmt.Println("----- ACME CHECK NOTIFY -----")
	ActivateID()
	fmt.Println("----- OPENID DNS SETUP -----")
	SetupOpenID()
	fmt.Println("----- IDENTITY DETAILS -----")
	GetIdentity()
	fmt.Println("----- SENDING MAIL -----")
	SendMail()

}

func ResetIdentityCredentials() {
	c, err := newClient()
	checkError(err)

	identity, err := c.ResetIdentityCredentials(id)
	checkError(err)

	printObj(identity)
}

func printObj(obj interface{}) {
	yamlbytes, err := yaml.Marshal(obj)
	checkError(err)

	fmt.Println(string(yamlbytes))

}

func askForValue(prompt, value string) string {
	reader := bufio.NewReader(os.Stdin)
	if len(value) > 0 {
		fmt.Printf("%s [%s]: ", prompt, value)
	} else {
		fmt.Printf("%s: ", prompt)
	}

	text, _ := reader.ReadString('\n')
	text = strings.Replace(text, "\r\n", "", -1)
	text = strings.Replace(text, "\n", "", -1)
	text = strings.Trim(text, " ")
	if len(text) < 1 && len(value) > 0 {
		return value
	}
	return text
}
