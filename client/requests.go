package client

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/denic-id/aim-client/models"
	"gitlab.com/denic-id/aim-client/utils"

	"gitlab.com/denic-id/aim-client/crypto"
)

func getBodyString(resp *http.Response) string {
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err.Error()
	}
	return string(body)
}

func getBodyBytes(resp *http.Response) []byte {
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil
	}
	return body
}

func (c *Client) newRequest(method, url, payload string, withKeyID, embedKey bool) (*http.Request, error) {
	req, err := http.NewRequest(method, url, strings.NewReader(payload))
	if err != nil {
		return nil, err
	}
	s, err := crypto.NewSigner(c.key)
	if err != nil {
		return nil, err
	}

	s.SetOption("b64", false)
	s.SetOption("url", url)
	s.SetOption("method", method)

	if withKeyID {
		s.SetOption("kid", c.key.KeyID)
	}
	s.EmbedKey(true)

	jws, err := s.SignPayload(payload)
	if err != nil {
		return nil, err
	}

	header, _, err := s.DetachPayload(jws)

	req.Header.Add("jws-detached-signature", header)
	req.Header.Add("Content-Type", "application/json")

	return req, nil
}

func (c *Client) existsAtAuthority() (bool, *http.Response, error) {
	url := fmt.Sprintf("%s/%s/%s", c.config.AimURL, "agents", c.key.KeyID)
	r, err := c.newRequest(http.MethodGet, url, "", true, false)
	if err != nil {
		return false, nil, err
	}

	resp, err := c.httpClient.Do(r)
	if err != nil {
		return false, nil, err
	}

	if resp.StatusCode != 200 {
		return false, resp, nil
	}

	return true, resp, nil
}

func (c *Client) postNewAgent() (*models.Agent, error) {
	url := fmt.Sprintf("%s/%s", c.config.AimURL, "agents")
	var agent models.Agent
	agent.Name = c.config.Client.Name
	agent.FullName = c.config.Client.FullName
	agent.Email = c.config.Client.Email
	agent.IssuerURL = c.config.Client.IssuerURL
	payloadbytes, err := json.Marshal(agent)
	if err != nil {
		return nil, err
	}

	r, err := c.newRequest(http.MethodPost, url, string(payloadbytes), false, true)
	if err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(r)
	if err != nil {
		return nil, err
	}

	body := getBodyBytes(resp)

	if resp.StatusCode != 201 {
		return nil, FailedCreateAgentError.Args(string(body)).Make()
	}
	if err := json.Unmarshal(body, &agent); err != nil {
		return nil, FailedCreateAgentError.Args(err.Error()).Make()
	}
	return &agent, nil
}

func (c *Client) putAgent(agent *models.Agent) error {
	payloadbytes, err := json.Marshal(agent)
	if err != nil {
		return GenericClientError.Args(err.Error()).Make()
	}

	url := fmt.Sprintf("%s/%s", c.config.AimURL, "agents/"+c.config.Client.ID)

	r, err := c.newRequest(http.MethodPut, url, string(payloadbytes), true, false)
	if err != nil {
		return HttpClientError.Args(err.Error()).Make()
	}

	resp, err := c.httpClient.Do(r)
	if err != nil {
		return HttpClientError.Args(err.Error()).Make()
	}

	body := getBodyBytes(resp)

	if resp.StatusCode != 200 {
		return FailedUpdateAgentError.Args(string(body)).Make()
	}

	err = json.Unmarshal(body, agent)
	if err != nil {
		return FailedUpdateAgentError.Args(string(body)).Make()
	}
	return nil

}

func (c *Client) postNewIdentityAuthz(identifier, locale string) (*models.IdentityAuthz, error) {
	var idauthz models.IdentityAuthzRequest
	idauthz.Identifier = identifier
	idauthz.Locale = locale

	url := fmt.Sprintf("%s/%s", c.config.AimURL, "authz")

	payloadbytes, err := json.Marshal(idauthz)
	if err != nil {
		return nil, GenericClientError.Args(err.Error()).Make()
	}

	r, err := c.newRequest(http.MethodPost, url, string(payloadbytes), true, true)
	if err != nil {
		return nil, HttpClientError.Args(err.Error()).Make()
	}

	resp, err := c.httpClient.Do(r)
	if err != nil {
		return nil, HttpClientError.Args(err.Error()).Make()
	}

	body := getBodyBytes(resp)

	if resp.StatusCode != 201 {
		return nil, FailedCreateIdAuthzError.Args(string(body)).Make()
	}
	var idauthzresponse models.IdentityAuthz
	if err := json.Unmarshal(body, &idauthzresponse); err != nil {
		return nil, FailedCreateIdAuthzError.Args(err.Error()).Make()
	}
	return &idauthzresponse, nil

}

func (c *Client) getIdentityAuthz(id string) (*models.IdentityAuthz, error) {
	authz, err := utils.AuthzFromYamlFile(c.config.AuthzDir, id)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s/%s/%d", c.config.AimURL, "authz", authz.ID)

	r, err := c.newRequest(http.MethodGet, url, "", true, false)
	if err != nil {
		return nil, HttpClientError.Args(err.Error()).Make()
	}

	resp, err := c.httpClient.Do(r)
	if err != nil {
		return nil, HttpClientError.Args(err.Error()).Make()
	}

	body := getBodyBytes(resp)

	if resp.StatusCode != 200 {
		return nil, FailedCreateIdAuthzError.Args(string(body)).Make()
	}
	var idauthzresponse models.IdentityAuthz
	if err := json.Unmarshal(body, &idauthzresponse); err != nil {
		return nil, FailedCreateIdAuthzError.Args(err.Error()).Make()
	}
	return &idauthzresponse, nil

}

func (c *Client) postAcmeSetUp(id string) (*models.IdentityAuthz, error) {
	authz, err := utils.AuthzFromYamlFile(c.config.AuthzDir, id)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s/%s/%d", c.config.AimURL, "authz", authz.ID)

	r, err := c.newRequest(http.MethodPost, url, "", true, false)
	if err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(r)
	if err != nil {
		return nil, err
	}

	body := getBodyBytes(resp)

	if resp.StatusCode != 200 {
		return nil, FailedIdAuthzSetUpError.Args(string(body)).Make()
	}
	var idauthzresponse models.IdentityAuthz
	if err := json.Unmarshal(body, &idauthzresponse); err != nil {
		return nil, FailedIdAuthzSetUpError.Args(err.Error()).Make()
	}
	return &idauthzresponse, nil

}

func (c *Client) getIdentity(id string) (*models.Identity, error) {

	url := fmt.Sprintf("%s/%s/%s", c.config.AimURL, "identities", id)
	r, err := c.newRequest(http.MethodGet, url, "", true, false)
	if err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(r)
	if err != nil {
		return nil, err
	}

	body := getBodyBytes(resp)

	if resp.StatusCode != 200 {
		return nil, GetIdentityError.Args(string(body)).Make()
	}
	var identity models.Identity
	if err := json.Unmarshal(body, &identity); err != nil {
		return nil, GetIdentityError.Args(err.Error()).Make()
	}
	return &identity, nil
}

func (c *Client) resetIdentityCredentials(id string) (*models.IdentitiesCredentialsResetResponse, error) {
	identity := new(models.Identity)
	err := utils.FromYamlFile(identity, c.config.AuthzDir+"/"+id+".yaml")
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s/%s/%s", c.config.AimURL, "identities", "credentialsReset")
	payload := fmt.Sprintf("{\"sub\":\"%s\"}", identity.Subject)
	r, err := c.newRequest(http.MethodPost, url, payload, true, false)

	if err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(r)
	if err != nil {
		return nil, err
	}

	body := getBodyBytes(resp)

	if resp.StatusCode != 200 {
		return nil, GetIdentityError.Args(string(body)).Make()
	}
	var respObj models.IdentitiesCredentialsResetResponse
	if err := json.Unmarshal(body, &respObj); err != nil {
		return nil, GetIdentityError.Args(err.Error()).Make()
	}
	return &respObj, nil
}

func (c *Client) getOwnAgent() (*models.Agent, error) {
	url := fmt.Sprintf("%s/%s/%s", c.config.AimURL, "agents", c.config.Client.ID)
	r, err := c.newRequest(http.MethodGet, url, "", true, false)
	if err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(r)
	if err != nil {
		return nil, err
	}

	body := getBodyBytes(resp)

	if resp.StatusCode != 200 {
		return nil, GetAgentError.Args(fmt.Sprintf("%s -> %s", url, string(body))).Make()
	}
	var respObj models.Agent
	if err := json.Unmarshal(body, &respObj); err != nil {
		return nil, GetAgentError.Args(err.Error()).Make()
	}
	return &respObj, nil
}
