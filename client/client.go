package client

import (
	gcrypto "crypto"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"net/http"
	"os"

	"gitlab.com/denic-id/aim-client/config"
	"gitlab.com/denic-id/aim-client/crypto"
	"gitlab.com/denic-id/aim-client/models"
	"gitlab.com/denic-id/aim-client/utils"
	jose "gopkg.in/square/go-jose.v2"
)

type Client struct {
	httpClient  *http.Client
	key         *jose.JSONWebKey
	config      *config.AppConfig
	configfile  string
	id          string
	initialized bool
}

func Init(appconfig *config.AppConfig, configfile string) (*Client, error) {
	c := new(Client)
	key := &jose.JSONWebKey{}
	var err error
	if _, err := os.Stat(appconfig.Client.KeyFile); os.IsNotExist(err) {

		key, err = crypto.CreateJwk(crypto.TYPE_ECDSA)
		if err != nil {
			return nil, err
		}
		if keyerr := crypto.WriteKeyToFile(key, appconfig.Client.KeyFile); keyerr != nil {
			return nil, keyerr
		}

	} else {
		key, err = crypto.ReadKeyFromFile(appconfig.Client.KeyFile)
		if err != nil {
			return nil, err
		}
	}
	c.config = appconfig
	c.key = key
	c.initialized = true
	c.httpClient = new(http.Client)
	c.configfile = configfile

	exists, _, err := c.existsAtAuthority()
	if err != nil {
		return nil, err
	}

	if !exists {
		err := c.CreateAtAuthority()
		if err != nil {
			return nil, err
		}
	} else {
		return nil, fmt.Errorf("Client with this key already exists at authority")
	}
	return c, nil
}

func (c *Client) GetConfig() config.AppConfig {
	return *c.config
}

func NewFromConfig(appconfig *config.AppConfig, configfile string) (*Client, error) {
	c := new(Client)
	key, err := crypto.ReadKeyFromFile(appconfig.Client.KeyFile)
	if err != nil {
		return nil, err
	}

	c.key = key
	c.config = appconfig
	c.initialized = true
	c.httpClient = new(http.Client)
	return c, nil
}

func (c *Client) CreateAtAuthority() error {
	if !c.initialized {
		return NotInitializedError
	}

	exists, _, err := c.existsAtAuthority()
	if err != nil {
		return err
	}

	if exists {
		return nil
	}

	agent, err := c.postNewAgent()
	if err != nil {
		return err
	}

	err = c.updateClientKeyAfterRegistration(agent.ID)
	if err != nil {
		return err
	}
	c.config.Client.ID = agent.ID
	err = utils.ToYamlFile(c.config, c.configfile)
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) UpdateAgent(name, fullname, email string) (*models.Agent, error) {
	agent := &models.Agent{}
	agent.Name = c.config.Client.Name
	agent.FullName = c.config.Client.FullName
	agent.Email = c.config.Client.Email
	agent.IssuerURL = c.config.Client.IssuerURL

	if len(name) > 0 {
		agent.Name = name
	}

	if len(fullname) > 0 {
		agent.FullName = fullname
	}

	if len(email) > 0 {
		agent.Email = email
	}

	err := c.putAgent(agent)
	if err != nil {
		return nil, err
	}
	return agent, nil
}

func (c *Client) updateClientKeyAfterRegistration(kid string) error {
	c.key.KeyID = kid
	return crypto.WriteKeyToFile(c.key, c.config.Client.KeyFile)
}

func (c *Client) CreateAndSaveIdentityAuthz(id, locale, email string) (*models.IdentityAuthz, error) {
	authz, err := c.postNewIdentityAuthz(id, locale)
	if err != nil {
		return nil, err
	}

	authz.Email = email
	err = utils.ToYamlFile(authz, c.config.AuthzDir+"/"+id+".authz.yaml")
	if err != nil {
		return nil, err
	}

	return authz, nil
}

func (c *Client) CreateAcmeChallenge(token string) (string, error) {
	thumbprint, err := c.key.Thumbprint(gcrypto.SHA256)
	if err != nil {
		return "", err
	}

	thumbprintStr := base64.RawURLEncoding.EncodeToString(thumbprint)
	hasher := sha256.New()
	if _, err := hasher.Write([]byte(token + "." + thumbprintStr)); err != nil {
		return "", err
	}
	hash := hasher.Sum(nil)
	hashStr := base64.RawURLEncoding.EncodeToString(hash)

	return hashStr, nil
}

func (c *Client) GetIdentityAuthz(id string) (*models.IdentityAuthz, error) {

	existingAuthz, err := utils.AuthzFromYamlFile(c.config.AuthzDir, id)
	if err != nil {
		existingAuthz = &models.IdentityAuthz{Email: ""}
	}

	authzAtAuthority, err := c.getIdentityAuthz(id)
	if err != nil {
		return nil, err
	}

	authzAtAuthority.Email = existingAuthz.Email
	err = utils.ToYamlFile(authzAtAuthority, c.config.AuthzDir+"/"+id+".authz.yaml")
	if err != nil {
		return nil, err
	}
	return authzAtAuthority, nil
}

func (c *Client) PostAcmeSetupDone(id string) (*models.IdentityAuthz, error) {
	authzAtAuthority, err := c.postAcmeSetUp(id)
	if err != nil {
		return nil, err
	}
	return authzAtAuthority, nil
}

func (c *Client) GetIdentity(id string) (*models.Identity, error) {
	identity, err := c.getIdentity(id)
	if err != nil {
		return nil, err
	}

	authz, err := utils.AuthzFromYamlFile(c.config.AuthzDir, id)
	identity.Email = authz.Email

	err = utils.ToYamlFile(identity, c.config.AuthzDir+"/"+id+".yaml")
	if err != nil {
		return nil, err
	}

	return identity, nil
}

func (c *Client) ResetIdentityCredentials(id string) (*models.IdentitiesCredentialsResetResponse, error) {
	return c.resetIdentityCredentials(id)
}

func (c *Client) GetOwnAgent() (*models.Agent, error) {
	return c.getOwnAgent()
}
