package client

import (
	"fmt"
	"testing"

	"gitlab.com/denic-id/aim-client/utils"

	"gitlab.com/denic-id/aim-client/config"

	"github.com/dchest/uniuri"
	"github.com/stretchr/testify/assert"
)

var appconfig = &config.AppConfig{
	Client: &config.ClientConfig{
		KeyFile:  "testkey.json",
		Name:     fmt.Sprintf("denic-test-%s", uniuri.NewLen(8)),
		FullName: "client-full-name",
		Email:    "mail@client",
	},
	AimURL: "https://id.staging.denic.de/aim/v1",
}

func TestNewClient(t *testing.T) {

	t.Run("init a new client", func(t *testing.T) {
		c, err := Init(appconfig)
		if !assert.NoError(t, err, "new client must be initialized.") {
			return
		}
		if !assert.Equal(t, true, c.key.Valid(), "client must have a valid key.") {
			return
		}
	})

	t.Run("create a new client", func(t *testing.T) {
		_, err := NewFromConfig(appconfig)
		if !assert.NoError(t, err, "New client must be created") {
			return
		}
	})

	t.Run("exists at authority", func(t *testing.T) {
		c, err := NewFromConfig(appconfig)
		if !assert.NoError(t, err, "New client must be created") {
			return
		}
		exists, _, err := c.existsAtAuthority()
		if !assert.NoError(t, err, "check client for existance at authority must not fail.") {
			return
		}

		if !assert.Equal(t, true, exists, "client must exist at authority") {
			return
		}
	})

	t.Run("create at authority", func(t *testing.T) {
		appconfig.Client.Name = fmt.Sprintf("denic-test-%s", uniuri.NewLen(8))
		c, err := NewFromConfig(appconfig)
		if !assert.NoError(t, err, "New client must be created") {
			return
		}

		err = c.CreateAtAuthority()
		if !assert.NoError(t, err, "Client must be created at the authority.") {
			return
		}

		exists, _, err := c.existsAtAuthority()
		if !assert.NoError(t, err, "check client for existance at authority must not fail.") {
			return
		}

		if !assert.Equal(t, true, exists, "client must exist at authority") {
			return
		}
	})
}

func TestClientExists(t *testing.T) {
	t.Run("existing at authority", func(t *testing.T) {
		//appconfig.Client.Name = fmt.Sprintf("denic-test-%s", uniuri.NewLen(8))
		c, err := NewFromConfig(appconfig)
		if !assert.NoError(t, err, "New client must be created") {
			return
		}

		exists, _, err := c.existsAtAuthority()
		if !assert.NoError(t, err, "check client for existance at authority must not fail.") {
			return
		}

		if !assert.Equal(t, true, exists, "client must exist at authority") {
			return
		}
	})
}

func TestClientPostNewIdAuthz(t *testing.T) {
	t.Run("create new authz", func(t *testing.T) {
		c, err := NewFromConfig(appconfig)
		if !assert.NoError(t, err, "New client must be created") {
			return
		}
		id := fmt.Sprintf("%s.foobar.bla", uniuri.NewLen(8))
		authz, err := c.postNewIdentityAuthz(id, "de")
		if !assert.NoError(t, err, "post new id must not fail.") {
			return
		}

		challenge, err := c.CreateAcmeChallenge(authz.Challenge.Token)
		fmt.Println(challenge)
	})

	id := fmt.Sprintf("%s.foobar.bla", uniuri.NewLen(8))
	t.Run("create new authz and write yaml", func(t *testing.T) {
		appconfig.AuthzDir = "."
		c, err := NewFromConfig(appconfig)
		if !assert.NoError(t, err, "New client must be created") {
			return
		}

		authz, err := c.postNewIdentityAuthz(id, "de")
		if !assert.NoError(t, err, "post new id must not fail.") {
			return
		}

		err = utils.AuthzToYamlFile(authz, ".")
		if !assert.NoError(t, err, "Auth file must be created.") {
			return
		}

		challenge, err := c.CreateAcmeChallenge(authz.Challenge.Token)
		fmt.Println(challenge)
	})

	t.Run("create and save new authz", func(t *testing.T) {
		appconfig.AuthzDir = "."
		id := fmt.Sprintf("%s.foobar.bla", uniuri.NewLen(8))
		c, err := NewFromConfig(appconfig)
		if !assert.NoError(t, err, "New client must be created") {
			return
		}

		authz, err := c.CreateAndSaveIdentityAuthz(id, "de")
		if !assert.NoError(t, err, "post new id must not fail.") {
			return
		}

		challenge, err := c.CreateAcmeChallenge(authz.Challenge.Token)
		fmt.Println(challenge)
	})

	t.Run("load authz from file and get authz", func(t *testing.T) {
		appconfig.AuthzDir = "."
		c, err := NewFromConfig(appconfig)
		if !assert.NoError(t, err, "New client must be created") {
			return
		}

		authz, err := utils.AuthzFromYamlFile(".", id)
		if !assert.NoError(t, err, "authz must be loaded from file") {
			return
		}

		authzAtAuthority, err := c.getIdentityAuthz(authz.Identifier)
		if !assert.NoError(t, err, "must get authz from authority") {
			return
		}

		if !assert.Equal(t, authz.Challenge.Token, authzAtAuthority.Challenge.Token, "Authz must be equal") {
			return
		}
	})
}

func TestClientPostIdAuthzReady(t *testing.T) {
	appconfig.AuthzDir = "."
	id := fmt.Sprintf("%s.foobar.bla", uniuri.NewLen(8))
	c, err := NewFromConfig(appconfig)
	if !assert.NoError(t, err, "New client must be created") {
		return
	}

	_, err = c.CreateAndSaveIdentityAuthz(id, "de")
	if !assert.NoError(t, err, "post new id must not fail.") {
		return
	}

	t.Run("post id ready", func(t *testing.T) {
		authz, err := c.postAcmeSetUp(id)
		if !assert.NoError(t, err, "must get authz from authority") {
			return
		}

		if assert.Equal(t, nil, authz.Challenge.Validated, "Fucked up.") {
			return
		}
	})
}
