package client

import "github.com/sbreitf1/errors"

var (
	GenericClientError       = errors.New("GenericClientError").Msg("Error: %s")
	NewAgentError            = errors.New("NewClientError").Msg("failed to create new client: %s")
	GetAgentError            = errors.New("GetAgentError").Msg("failed to get agent: %s")
	NotInitializedError      = errors.New("NotInitializedError").Msg("client is not initialized.").Make()
	FailedCreateAgentError   = errors.New("FailedCreateClientError").Msg("failed to create agent at authority: %s")
	FailedCreateIdAuthzError = errors.New("FailedCreateIdAuthz").Msg("failed to create authz for identity: %s")
	HttpClientError          = errors.New("HttpClientError").Msg("http request failed: %s")
	FailedIdAuthzSetUpError  = errors.New("FailedIdAuthzSetUpError").Msg("failed to post id set up: %s")
	GetIdentityError         = errors.New("GetIdentityError").Msg("failed to get identity: %s")
	FailedUpdateAgentError   = errors.New("FailedUpdateAgentError").Msg("failed to update the agent: %s")
)
