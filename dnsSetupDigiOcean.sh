#!/bin/bash

rname=$1
rtype=$2
rdata=$3
DOMAIN=iostat.de

# get the dns label without domain and trailing dot
RNAME=${rname%${DOMAIN}}
RNAME=${RNAME%.}

usage() {
    echo "$0 <name> <type> <data>"
    echo "  name - The name of the DNS record. Example: _acme-challenge.myid.example.org"
    echo "  type - The type of the RR. Example: TXT"
    echo "  data - The data for the record. Example: \"lshenl1233-jslfj\""
    exit 1
}

RECORD_ID=$(doctl compute domain records list ${DOMAIN} --no-header | grep "${RNAME}" | awk '{print $1}')
[[ -n "${RECORD_ID}" ]] && {
    [[ $(echo ${RECORD_ID} | wc -w) -gt 1 ]] && { echo "More than one record exists for ${RNAME}. Please cleanup first."; exit 1;}
    echo "we need to update record ${RECORD_ID}"
    doctl compute domain records update ${DOMAIN} --record-id=${RECORD_ID} --record-type=${rtype} --record-ttl=300 --record-name=${RNAME} --record-data=\"${rdata}\"
    [[ $? -eq 0 ]] && { exit 0; } || { echo "Failed to update record!"; exit 1; }
} || {
    echo "we need to create a new record"
    doctl compute domain records create ${DOMAIN} --record-type=${rtype} --record-ttl=300 --record-name=${RNAME} --record-data=\"${rdata}\"
    [[ $? -eq 0 ]] && { exit 0; } || { echo "Failed to create record!"; exit 1; }
}

