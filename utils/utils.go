package utils

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/denic-id/aim-client/models"
	yaml "gopkg.in/yaml.v2"
)

func ToYaml(ia interface{}) ([]byte, error) {
	yamlbytes, err := yaml.Marshal(ia)
	if err != nil {
		return nil, err
	}
	return yamlbytes, nil
}

func ToYamlFile(ia interface{}, filename string) error {
	yamlbytes, err := ToYaml(ia)

	yf, err := os.OpenFile(filename, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}
	defer yf.Close()
	_, err = yf.Write(yamlbytes)
	if err != nil {
		return err
	}
	return nil
}

func FromYamlFile(ia interface{}, filename string) error {
	b, err := ioutil.ReadFile(filepath.Clean(filename))
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(b, ia)
	if err != nil {
		return err
	}
	return nil
}

func AuthzToYamlFile(ia *models.IdentityAuthz, basepath string) error {
	yamlbytes, err := yaml.Marshal(ia)
	if err != nil {
		return err
	}

	yf, err := os.OpenFile(basepath+"/"+ia.Identifier+".authz.yaml", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}
	defer yf.Close()
	_, err = yf.Write(yamlbytes)
	if err != nil {
		return err
	}
	return nil
}

func AuthzFromYamlFile(basepath, id string) (*models.IdentityAuthz, error) {
	filename := fmt.Sprintf(basepath + "/" + id + ".authz.yaml")

	b, err := ioutil.ReadFile(filepath.Clean(filename))
	if err != nil {
		return nil, err
	}

	authz := new(models.IdentityAuthz)
	err = yaml.Unmarshal(b, authz)
	if err != nil {
		return nil, err
	}
	return authz, nil
}

func MakeDirs(dirname string) error {
	if err := os.MkdirAll(filepath.Clean(dirname), 0700); err != nil {
		return err
	}
	return nil
}
