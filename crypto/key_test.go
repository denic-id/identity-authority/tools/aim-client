package crypto

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateJwk(t *testing.T) {
	t.Run("create ecdsa jwk", func(t *testing.T) {
		key, err := createEcdsaJwk()
		assert.NoError(t, err, "must create an ecdsa key")
		assert.Equal(t, "sig", key.Use, "key usage must be sig")
		assert.Equal(t, "ES512", key.Algorithm, "key alg must be ES512")
	})

	t.Run("create rsa jwk", func(t *testing.T) {
		key, err := createEcdsaJwk()
		assert.NoError(t, err, "must create an ecdsa key")
		assert.Equal(t, "sig", key.Use, "key usage must be sig")
	})

	t.Run("CreateJwk of type ecdsa", func(t *testing.T) {
		key, err := CreateJwk(TYPE_ECDSA)
		assert.NoError(t, err, "Must create an ecdsa key.")
		assert.Equal(t, "sig", key.Use, "usage of key must be sig.")
		assert.Equal(t, "ES512", key.Algorithm, "key alg must be ES512")

	})

	t.Run("CreateJwk of type rsa", func(t *testing.T) {
		key, err := CreateJwk(TYPE_RSA)
		assert.NoError(t, err, "Must create an rsa key.")
		assert.Equal(t, "sig", key.Use, "usage of key must be sig.")
		assert.Equal(t, "RS512", key.Algorithm, "alg of key must be RS512")
	})

	t.Run("Fail create RSA Key", func(t *testing.T) {
		_, err := CreateJwk("rsab")
		assert.Error(t, err, "Must NOT create an rsa key.")
	})

}

func TestReadWriteKeyFile(t *testing.T) {

	key, err := CreateJwk(TYPE_ECDSA)
	assert.NoError(t, err, "Must create an ecdsa key.")

	t.Run("write key to file", func(t *testing.T) {
		err := WriteKeyToFile(key, "testkey.json")
		assert.NoError(t, err, "key must be written to file")
	})

	t.Run("fail to write key to file", func(t *testing.T) {
		err := WriteKeyToFile(key, "/root/testkey.json")
		assert.Error(t, err, "key must NOT be written to file")
	})

	t.Run("read key from file", func(t *testing.T) {
		key, err := ReadKeyFromFile("testkey.json")
		assert.NoError(t, err, "must read key from file")
		assert.Equal(t, "ES512", key.Algorithm, "key must be of type EC")
	})

	rmerr := os.Remove("testkey.json")
	assert.NoError(t, rmerr, "testkey.json must be deleted")

}
