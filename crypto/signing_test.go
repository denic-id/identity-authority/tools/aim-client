package crypto

import (
	"fmt"
	"testing"

	jose "gopkg.in/square/go-jose.v2"

	"github.com/stretchr/testify/assert"
)

func TestSigning(t *testing.T) {

	key, err := CreateJwk(TYPE_ECDSA)
	assert.NoError(t, err, "a ecdsa jwk must be created")

	t.Run("create signer", func(t *testing.T) {
		s, err := NewSigner(key)
		assert.NoError(t, err, "a signer must be created")

		assert.Equal(t, key, s.key, "signer must use the created key")
	})

	t.Run("sign payload", func(t *testing.T) {
		s, err := NewSigner(key)
		assert.NoError(t, err, "a signer must be created")

		payload := "test payload"

		jws, err := s.SignPayload(payload)
		assert.NoError(t, err, "payload must be signed by signer")

		verified, err := jws.Verify(key.Public().Key)
		assert.NoError(t, err, "signature must verify against the key an payload")
		assert.Equal(t, payload, string(verified), "verified payload must be equal input.")
	})

	t.Run("embed key in sign", func(t *testing.T) {
		s, err := NewSigner(key)
		assert.NoError(t, err, "a signer must be created")

		payload := "test payload embed"

		s.EmbedKey(true)
		s.SetOption("b64", false)
		fmt.Printf("%#v\n", s.options)

		jws, err := s.SignPayload(payload)
		assert.NoError(t, err, "payload must be signed by signer")

		verified, err := jws.Verify(key.Public().Key)
		assert.NoError(t, err, "signature must verify against the key an payload")

		if !assert.Equal(t, payload, string(verified), "payload must be equal input.") {
			return
		}

		header, p, err := s.DetachPayload(jws)
		if !assert.NoError(t, err, "must split payload and signature from jws") {
			return
		}

		if !assert.Equal(t, payload, p, "payload must be equal input.") {
			return
		}

		djws, err := jose.ParseSigned(header)
		if !assert.NoError(t, err, "must get detached signature from jws") {
			return
		}

		embeddedKey := djws.Signatures[0].Protected.JSONWebKey
		if !assert.Equal(t, true, embeddedKey.IsPublic(), "Embedded key must be public.") {
			return
		}
	})
}
