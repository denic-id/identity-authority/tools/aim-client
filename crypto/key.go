package crypto

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/google/uuid"
	jose "gopkg.in/square/go-jose.v2"
)

const (
	TYPE_RSA   = "rsa"
	TYPE_ECDSA = "ecdsa"
)

// CreateJwk creates a new JWK of type keytype and stores it to filename
// keytype can be "rsa" or "ecdsa"
func CreateJwk(keytype string) (*jose.JSONWebKey, error) {
	key := new(jose.JSONWebKey)
	var err error
	switch keytype {
	case TYPE_RSA:
		key, err = createRsaJwk()
		if err != nil {
			return nil, err
		}
	case TYPE_ECDSA:
		key, err = createEcdsaJwk()
		if err != nil {
			return nil, err
		}
	default:
		return nil, UnkownKeyTypeError.Args(keytype).Make()
	}

	return key, nil
}

func createEcdsaJwk() (*jose.JSONWebKey, error) {
	privkey, keyerr := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	if keyerr != nil {
		return nil, GenerateKeyError.Args("ecdsa", keyerr.Error()).Make()
	}
	kid := uuid.New().String()

	key := &jose.JSONWebKey{Key: privkey, KeyID: kid, Algorithm: "ES512", Use: "sig"}

	return key, nil
}

func createRsaJwk() (*jose.JSONWebKey, error) {
	privkey, keyerr := rsa.GenerateKey(rand.Reader, 2048)
	if keyerr != nil {
		return nil, GenerateKeyError.Args("rsa", keyerr.Error()).Make()
	}

	kid := uuid.New().String()
	key := &jose.JSONWebKey{Key: privkey, KeyID: kid, Algorithm: "RS512", Use: "sig"}

	return key, nil
}

func WriteKeyToFile(key *jose.JSONWebKey, filename string) error {
	jsonbuf, err := key.MarshalJSON()
	if err != nil {
		return MarshallKeyError
	}

	keyfilehandle, err := os.OpenFile(filepath.Clean(filename), os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0600)
	defer keyfilehandle.Close()
	if err != nil {
		return WriteKeyError.Args(err.Error()).Make()
	}

	_, err = keyfilehandle.Write(jsonbuf)
	if err != nil {
		return WriteKeyError.Args(err.Error()).Make()
	}

	return nil
}

func ReadKeyFromFile(filename string) (*jose.JSONWebKey, error) {
	// we load the keys from file
	privkeybytes, err := ioutil.ReadFile(filepath.Clean(filename))
	if err != nil {
		return nil, err
	}

	JWK := new(jose.JSONWebKey)
	err = JWK.UnmarshalJSON(privkeybytes)
	if err != nil {
		return nil, err
	}
	return JWK, nil
}

func ReadKeyFromJson(jsonbytes []byte) (*jose.JSONWebKey, error) {
	JWK := new(jose.JSONWebKey)
	err := JWK.UnmarshalJSON(jsonbytes)
	if err != nil {
		return nil, err
	}
	return JWK, nil
}
