package crypto

import "github.com/sbreitf1/errors"

var (
	UnkownKeyTypeError = errors.New("UnkownKeyType").Msg("cannot create key of type %s")
	GenerateKeyError   = errors.New("GenerateKeyError").Msg("failed to generate %s key: %s")
	WriteKeyError      = errors.New("WriteKeyError").Msg("failed to write key to file: %s")
	MarshallKeyError   = errors.New("MarshallKeyError").Msg("failed to marshall key to json").Make()
)
