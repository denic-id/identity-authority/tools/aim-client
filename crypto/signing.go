package crypto

import (
	"encoding/base64"
	"strings"

	jose "gopkg.in/square/go-jose.v2"
)

type Signer struct {
	key     *jose.JSONWebKey
	options *jose.SignerOptions
}

func NewSigner(key *jose.JSONWebKey) (*Signer, error) {
	s := new(Signer)
	s.key = key
	s.options = new(jose.SignerOptions)
	return s, nil
}

func (s *Signer) SetOptions(options *jose.SignerOptions) {
	s.options = options
}

func (s *Signer) SetOption(key jose.HeaderKey, data interface{}) {
	s.options.WithHeader(key, data)
}

func (s *Signer) EmbedKey(mode bool) {
	s.options.EmbedJWK = mode
}

func (s *Signer) SignPayload(payload string) (*jose.JSONWebSignature, error) {

	signer, err := jose.NewSigner(
		jose.SigningKey{
			Algorithm: jose.SignatureAlgorithm(s.key.Algorithm),
			Key:       s.key},
		s.options)
	if err != nil {
		return nil, err
	}

	jws, err := signer.Sign([]byte(payload))
	if err != nil {
		return nil, err
	}

	return jws, nil
}

func (s *Signer) DetachPayload(jws *jose.JSONWebSignature) (string, string, error) {
	serialized, err := jws.CompactSerialize()
	if err != nil {
		return "", "", err
	}
	parts := strings.Split(serialized, ".")
	if len(parts) != 3 {
		return "", "", err
	}

	payload, err := base64.RawURLEncoding.DecodeString(parts[1])
	if err != nil {
		return "", "", err
	}
	xheader := strings.Join([]string{parts[0], parts[2]}, "..")
	return xheader, string(payload), nil
}
