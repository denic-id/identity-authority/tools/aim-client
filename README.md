# AIM Client

This is a commandline client for interacting with the Agent-Identity-Management endpoint in DENIC ID.  
With this client, you can add identities to the denic-id authority. 

# Install

## From source
```bash
go get gitlab.com/denic-id/aim-client
go install gitlab.com/denic-id/aim-client/cmd/aim-client
```

## Binary download

Ready to go binaries can be found [here](https://denic-id.gitlab.io/aim-client)

The windows binaries are not fully tested yet.

# Usage

```
aim-client --help
```

## init the client

Decide where the configfile, keyfile and yamls of identities and authz will be saved.  
The client will not create any folders for you. Please create them in advance.

```
aim-client init
```

## autocompletion on linux

```
source <(aim-client --completion-script-bash)
```

## create an authorization for an identity

```
aim-client create authz myid.example.org en
aim-client get authz myid.example.org
```

## signal that the acme-challenge is solved

```
aim-client activate id myid.example.org
```

## get the password setup url for the activated id
```
aim-client get id myid.example.org
```

# Hooks for mail and dns

You can invoke external calls to hook commands to setup the DNS entries and send a mail to the identity owner.

## DNS Hook

The DNS hook will be called with the following positional arguments:
* rname - Name of the DNS record ( `_acme-challenge.myid.zone.tld` )
* rtype - Type of the DNS record ( `TXT` )
* rdata - Data of the record ( `lwhfhfueh-hwlus/slenn-ejj` )

Example call of a DNS hook:
```
my-dns-hook _acme-challenge.myid.zone.tld TXT lwhfhfueh-hwlus/slenn-ejj
```

## Mail Hook

The mail hook will be called with the following positional arguments:
* identity - The identifier for the identity ( `myid.zone.tld` )
* email - The mail address of the owner of the identity ( `mail@somewhere.tld` )
* agentName - The name of the agent which set up the identity ( `Company Inc.` )
* link - The link which points to the password setup page ( `https://lkjsdf/lksjdlkfjsdf` )
* locale - The identity's desired locale ( `en` )
